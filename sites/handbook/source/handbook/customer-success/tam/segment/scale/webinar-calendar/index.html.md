---
layout: handbook-page-toc
title: "TAM Webinar Calendar"
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/tam/) for additional TAM-related handbook pages.

---

## Upcoming Webinars


### April 2022

We’d like to invite you to our free upcoming webinars during the month of April.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

### DevSecOps & Compliance 
#### April 26th, 2022 at 8:00-9:00 AM Pacific Time/3:00-4:00 PM UTC

We’ll cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.
 
[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_rZfoHvOySTe8YA0etieoZw)
