---
layout: handbook-page-toc
title: Home Page for Support's Ginkgo Group
description: Home Page for Support's Ginkgo Group
---

<!-- Search for all occurences of NAME and replace them with the group's name.
     Search for all occurences of URL HERE and replace them with the appropriate url -->

# Welcome to the home page of the Ginkgo group

Introductory text, logos, or whatever the group wants here

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Ginkgo resources

- Our Slack Channel: [spt_gg_ginkgo](https://gitlab.slack.com/archives/C0354N9B14G)
- Our Team: [Ginkgo Members](https://gitlab-com.gitlab.io/support/team/sgg.html?search=ginkgo)

## Ginkgo workflows and processes

